#include <stdio.h>
int main()
{
    int T=0, num=0;
    do{
        printf("Enter a number - ");
        scanf("%d", &num);
        if (num<=0)
            break;
        T+=num;
    }
    while(num>0);
    printf("Sum %d \n",T);
    return 0;
}
